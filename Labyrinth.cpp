#include<iostream>
#include <vector>
#include <fstream>
#include <algorithm>

using namespace std;

void printMaze(vector< vector <bool> > maze)
{
  for (int i = 0; i < maze.size(); i++)
  {
    for (int j = 0; j < maze[i].size(); j++)
    {
      cout << maze[i][j]<< "\t";
    }
    cout << endl;
  }
  cout << endl << endl;
}

void printPathInMaze(vector< vector <bool> > maze,vector< pair<int, int> >  &traversedPath)
{
  cout<<traversedPath.size()<<endl;

  vector< vector <int> > pmaze(maze.size(), vector<int>(maze[0].size(), -1));
  for(int i=0; i < traversedPath.size(); i++)
    pmaze[traversedPath[i].first][traversedPath[i].second] = i;

  for (int i = 0; i < pmaze.size(); i++)
  {
    for (int j = 0; j < pmaze[i].size(); j++)
    {
      if (pmaze[i][j] != -1) cout << pmaze[i][j];
      else cout << (maze[i][j] == true ? '.': '#');
    }
    cout << endl;
  }
  cout << endl << endl;
}

vector <vector<bool> > readMaze(char *filename) {

  vector <vector<bool> > maze;
  ifstream file(filename);
  std::string line;
  while (std::getline(file, line))
  {
    vector<bool> row(line.size());
    for (int i = 0; i < line.size(); i++)
    {
      if (line[i] == '#') row[i] = false;
      else row[i] = true;
    }
    maze.push_back(row);
  }
  return maze;
}

bool validPoint(pair<int, int> pos, vector <vector <bool> > const &maze, vector< pair<int, int> >  &traversedPath)
{
  int h = maze.size();
  int w = maze[0].size();

  //if it is a valid hole
  if (pos.first < 0 or pos.second < 0 or pos.first >= h or pos.second >= w) return false;
  if (maze[pos.first][pos.second] != true) return false;
  //if it is not traversed; So this algo will work well even when there are loops
  vector< pair<int, int> >::iterator iter = find(traversedPath.begin(),traversedPath.end(), pos);
  if (iter != traversedPath.end()) return false;

  //else it is valid
  return true;
}

vector< pair<int, int> > getValidNeighbors(pair<int, int> pos, vector <vector <bool> > const &maze, vector< pair<int, int> >  traversedPath)
{
  vector< pair<int, int> > validNeighbors;
  pair<int, int> p;
  //look into the 4 possibile direction starting from pos

  p = make_pair(pos.first-1, pos.second);
  if (validPoint(p, maze, traversedPath))
    validNeighbors.push_back(p);
  p = make_pair(pos.first+1, pos.second);
  if (validPoint(p, maze, traversedPath))
    validNeighbors.push_back(p);
  p = make_pair(pos.first, pos.second - 1);
  if (validPoint(p, maze, traversedPath))
    validNeighbors.push_back(p);
  p = make_pair(pos.first, pos.second + 1);
  if (validPoint(p, maze, traversedPath))
    validNeighbors.push_back(p);

  return validNeighbors;
}

vector< pair<int, int> >  longestPath(pair<int, int> pos, vector <vector <bool> > &maze, vector< pair<int, int> >  traversedPath)
{
  //visit this node and find the list of possible neighbors to visit
  traversedPath.push_back(pos);
  vector< pair<int, int> > validNeighbors = getValidNeighbors(pos, maze, traversedPath);

  //else there is atleast one possible path
  int max_len = 0;
  vector< pair<int, int> > max_sub_path;
  for(vector< pair<int, int> >::iterator ngi =validNeighbors.begin(); ngi != validNeighbors.end(); ++ngi)
  {
    vector< pair<int, int> > curr_path = longestPath(*ngi, maze, traversedPath);
    if (curr_path.size() > max_len) { max_len = curr_path.size(); max_sub_path = curr_path; }
  }

  vector< pair<int, int> > max_path;
  max_path.push_back(pos);
  max_path.insert(max_path.end(), max_sub_path.begin(), max_sub_path.end());
  return max_path;
}


void solveLabyrinth(char *filename)
{
  vector< vector <bool> > maze = readMaze(filename);


  //starting with all position in the boundaries
  int h = maze.size();
  int w = maze[0].size();
  int max_len = 0;
  vector< pair<int, int> > max_path;
  vector< pair<int, int> > curr_path;

  //horizontal
  for (int i = 0; i < w; i++)
  {
    if (maze[0][i] == true)
    {
      curr_path = longestPath(make_pair(0, i), maze, vector< pair<int, int> >());
      if (curr_path.size() > max_len) { max_len = curr_path.size(); max_path = curr_path; }
    }
    if (maze[h - 1][i] == true)
    {
      curr_path = longestPath(make_pair(h - 1, i), maze, vector< pair<int, int> >());
      if (curr_path.size() > max_len) { max_len = curr_path.size(); max_path = curr_path; }
    }
  }
  //vertical
  for (int i = 0; i < h; i++)
  {
    if (maze[i][0] == true)
    {
      curr_path = longestPath(make_pair(i, 0), maze, vector< pair<int, int> >());
      if (curr_path.size() > max_len) { max_len = curr_path.size(); max_path = curr_path; }
    }
    if (maze[i][w - 1] == true)
    {
      curr_path = longestPath(make_pair(i, w - 1), maze, vector< pair<int, int> >());
      if (curr_path.size() > max_len) { max_len = curr_path.size(); max_path = curr_path; }
    }
  }

  printPathInMaze(maze, max_path);
}

int main(int argc, char *argv[])
{
  if (argc < 2)
  {
    cout << "Please enter the input file";
  }
  solveLabyrinth(argv[1]);
  return 0;
}

